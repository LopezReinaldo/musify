package com.musify



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class StyleController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Style.list(params), model:[styleInstanceCount: Style.count()]
    }

    def show(Style styleInstance) {
        respond styleInstance
    }

    def create() {
        respond new Style(params)
    }

    @Transactional
    def save(Style styleInstance) {
        if (styleInstance == null) {
            notFound()
            return
        }

        if (styleInstance.hasErrors()) {
            respond styleInstance.errors, view:'create'
            return
        }

        styleInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'style.label', default: 'Style'), styleInstance.id])
                redirect styleInstance
            }
            '*' { respond styleInstance, [status: CREATED] }
        }
    }

    def edit(Style styleInstance) {
        respond styleInstance
    }

    @Transactional
    def update(Style styleInstance) {
        if (styleInstance == null) {
            notFound()
            return
        }

        if (styleInstance.hasErrors()) {
            respond styleInstance.errors, view:'edit'
            return
        }

        styleInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Style.label', default: 'Style'), styleInstance.id])
                redirect styleInstance
            }
            '*'{ respond styleInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Style styleInstance) {

        if (styleInstance == null) {
            notFound()
            return
        }

        styleInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Style.label', default: 'Style'), styleInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'style.label', default: 'Style'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
