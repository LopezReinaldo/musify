<%@ page import="com.musify.Artist" %>

<style>
	.select-multiple {
        width: 400px;
    }
</style>    

<div class="fieldcontain ${hasErrors(bean: artistInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="artist.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${artistInstance?.name}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: artistInstance, field: 'year', 'error')} required">
	<label for="year">
		<g:message code="artist.year.label" default="Year" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="year" type="number" value="${artistInstance.year}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: artistInstance, field: 'styles', 'error')} ">
	<label for="styles">
		<g:message code="artist.styles.label" default="Styles" />
		
	</label>
	<g:select name="styles" from="${com.musify.Style.list()}" multiple="multiple" optionKey="id" size="5" value="${artistInstance?.styles*.id}" class="select-multiple many-to-many"/>

</div>

<div class="fieldcontain ${hasErrors(bean: artistInstance, field: 'members', 'error')} ">
	<label for="members">
		<g:message code="artist.members.label" default="Members" />
		
	</label>
	<g:select name="members" from="${com.musify.People.list()}" multiple="multiple" optionKey="id" size="5" value="${artistInstance?.members*.id}" class="select-multiple many-to-many"/>

</div>

<div class="fieldcontain ${hasErrors(bean: artistInstance, field: 'related', 'error')} ">
	<label for="related">
		<g:message code="artist.related.label" default="Related" />
		
	</label>
	<g:select name="related" from="${com.musify.Artist.list() - artistInstance}" multiple="multiple" optionKey="id" size="5" value="${artistInstance?.related*.id}" class="select-multiple many-to-many"/>

</div>

