
<%@ page import="com.musify.Artist" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'artist.label', default: 'Artist')}" />
		<title>Artist List by Style</title>
	</head> 		
	<body>
		<a href="#list-artist" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
	<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
			</ul>
		</div>
		<div id="list-artist" class="content scaffold-list" role="main">
			<h1>Artist List by Style</h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:each in="${styles}" var="style">
					<h1>Style: ${style}</h1>
					<table>
					<thead>
							<tr>
								<g:sortableColumn property="name" title="Artists" />
							
							</tr>
						</thead>
						<tbody>
							<g:each in="${artistList}" status="i" var="artistInstance">
								<g:if test="${artistInstance.styles.contains(style)}">
									<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
								
										<td><g:link action="show" id="${artistInstance.id}">${fieldValue(bean: artistInstance, field: "name")}</g:link></td>
									</tr>
								</g:if>
			
							</g:each>
						</tbody>
					</table>
			</g:each>
		</div>
	</body>
</html>
