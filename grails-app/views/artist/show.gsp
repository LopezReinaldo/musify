
<%@ page import="com.musify.Artist" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'artist.label', default: 'Artist')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-artist" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-artist" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list artist">
			
				<g:if test="${artistInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="artist.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${artistInstance}" field="name"/></span>
					
				</li>
				</g:if>

				<g:if test="${artistInstance?.year}">
				<li class="fieldcontain">
					<span id="year-label" class="property-label"><g:message code="artist.year.label" default="Year" /></span>
					
						<span class="property-value" aria-labelledby="year-label"><g:fieldValue bean="${artistInstance}" field="year"/></span>
					
				</li>
				</g:if>

				<g:if test="${artistInstance?.styles}">
				<li class="fieldcontain">
					<span id="styles-label" class="property-label"><g:message code="artist.styles.label" default="Styles" /></span>
					
						<g:each in="${artistInstance.styles}" var="s">
						<span class="property-value" aria-labelledby="styles-label"><g:link controller="style" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
												
				<g:if test="${artistInstance?.members}">
				<li class="fieldcontain">
					<span id="members-label" class="property-label"><g:message code="artist.members.label" default="Members" /></span>
					
						<g:each in="${artistInstance.members}" var="m">
						<span class="property-value" aria-labelledby="members-label"><g:link controller="people" action="show" id="${m.id}">${m?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${artistInstance?.related}">
				<li class="fieldcontain">
					<span id="related-label" class="property-label"><g:message code="artist.related.label" default="Related" /></span>
					
						<g:each in="${artistInstance.related}" var="r">
						<span class="property-value" aria-labelledby="related-label"><g:link controller="artist" action="show" id="${r.id}">${r?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:artistInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${artistInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
