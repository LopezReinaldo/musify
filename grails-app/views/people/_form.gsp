<%@ page import="com.musify.People" %>



<div class="fieldcontain ${hasErrors(bean: peopleInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="people.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${peopleInstance?.name}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: peopleInstance, field: 'years', 'error')} required">
	<label for="years">
		<g:message code="people.years.label" default="Years" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="years" type="number" value="${peopleInstance.years}" required=""/>

</div>

