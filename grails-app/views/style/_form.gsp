<%@ page import="com.musify.Style" %>



<div class="fieldcontain ${hasErrors(bean: styleInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="style.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${styleInstance?.name}"/>

</div>

