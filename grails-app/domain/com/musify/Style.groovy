package com.musify

class Style {
	
	String name

    static constraints = {
    }
	
	String toString(){
		return "${name}"
	}

}
