package com.musify

class People {
	
	String name
	Integer years
	
    static constraints = {
    }
	
	String toString(){
		return "${name}, ${years}"
	}
}
