package com.musify

class Artist {
	
	String name
	Integer year
	
	static hasMany = [styles:Style, members: People, related: Artist]

    static constraints = {
    }
	
	String toString(){
		return "${name}, ${year}"
	}
}
